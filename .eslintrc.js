module.exports = {
  extends: ['last', 'prettier/react', 'plugin:react/recommended'], // extending recommended config and config derived from eslint-config-prettier
  plugins: ['prettier'], // activating esling-plugin-prettier (--fix stuff)
  env: {
    'browser': true,
    'node': true,
    'jasmine': true,
    'es6': true
  },
  rules: {
    'prettier/prettier': [ // customizing prettier rules (unfortunately not many of them are customizable)
      'error',
      {
        singleQuote: true,
        trailingComma: 'all',
      },
    ],
    eqeqeq: ['error', 'always'], // adding some custom ESLint rules
  },
  // https://github.com/yannickcr/eslint-plugin-react#configuration
  settings: {
    react: {
      version: 'detect', // React version. "detect" automatically picks the version you have installed.
                           // You can also use `16.0`, `16.3`, etc, if you want to override the detected value.
                           // default to latest and warns if missing
                           // It will default to "detect" in the future
    }
  }
};