const createStore = rootReducer => {
  let state;
  const listeners = [];

  const getState = () => state;

  const dispatch = action => {
    state = rootReducer(state, action);

    listeners.forEach(cb => cb());

    return action;
  };

  const subscribe = cb => {
    listeners.push(cb);

    return () => {
      const index = listeners.indexOf(cb);
      listeners.splice(index, 1);
    };
  };

  dispatch({});

  return { getState, dispatch, subscribe };
};

const rr = state => state;

const store = createStore(rr);

console.log(store);
console.log(store.getState());
