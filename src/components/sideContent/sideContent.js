import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import '../App.css';
import GameRules from './gameRules';

const SideContent = ({ playerOneScore, playerTwoScore }) => (
  <div className="sideContent">
    <div className="playerScoreboard">
      <div>Player One Score:</div>
      <div className="score">{playerOneScore}</div>
    </div>
    <div className="playerScoreboard">
      <div>Player Two Score:</div>
      <div className="score">{playerTwoScore}</div>
    </div>
    <GameRules />
  </div>
);

SideContent.propTypes = {
  playerOneScore: PropTypes.number.isRequired,
  playerTwoScore: PropTypes.number.isRequired,
};

const msp = state => {
  return {
    playerOneScore: state.playerOneScore,
    playerTwoScore: state.playerTwoScore,
  };
};

export default connect(msp)(SideContent);
