import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Modal from '@material-ui/core/Modal';
import Backdrop from '@material-ui/core/Backdrop';
import Fade from '@material-ui/core/Fade';
import Button from '@material-ui/core/Button';

const useStyles = makeStyles(theme => ({
  modal: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    margin: '5%',
  },
  paper: {
    backgroundColor: theme.palette.background.paper,
    border: '2px solid #000',
    boxShadow: theme.shadows[5],
    padding: theme.spacing(2, 4, 3),
    borderRadius: theme.spacing(2),
    outline: 'none',
  },
  margin: {
    margin: theme.spacing(1),
  },
}));

export default function GameRules() {
  const classes = useStyles();
  const [open, setOpen] = React.useState(false);

  const handleOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  return (
    <div>
      <Button
        size="large"
        color="secondary"
        variant="contained"
        className={classes.margin}
        onClick={handleOpen}
      >
        Game Rules
      </Button>
      <Modal
        aria-labelledby="transition-modal-title"
        aria-describedby="transition-modal-description"
        className={classes.modal}
        open={open}
        onClose={handleClose}
        closeAfterTransition
        BackdropComponent={Backdrop}
        BackdropProps={{
          timeout: 500,
        }}
      >
        <Fade in={open}>
          <div className={classes.paper}>
            <h2 id="transition-modal-title" className={classes.margin}>
              Rules of the game
            </h2>
            <p id="transition-modal-description" className={classes.margin}>
              The game is played with a full deck of 52 cards. Every player gets
              6 cards, and 4 cards are put open on the table. Every player can
              use one card from his hand, and match it with in points one or
              more cards on the table. The value of the cards is: ace is one or
              eleven 1-10 equals number on the card jack is twelve queen is
              thirteen king is fourteen When a player can not take any card from
              the table, he plays one of the cards from his hand onto the table,
              so others can use it to make a combination. When all six cards are
              played, all players will receive six more cards (or as many as
              there is left), until all cards from the stock are played. The
              player that made the last combination, will get any remaining
              cards from the table.{' '}
            </p>
            <h2 id="transition-modal-title" className={classes.margin}>
              Scoring
            </h2>
            <p id="transition-modal-description" className={classes.margin}>
              In the end, every 10, Jack, Queen, King and Ace equals one point.
              The 10 of diamonds is two points, and the two of clubs equals one
              point. All other 2-9 cards do not have points. The player that has
              the most cards, will get three extra points. In total there are 25
              points in one round.
            </p>
            <h2 id="transition-modal-title" className={classes.margin}>
              Tabla
            </h2>
            <p id="transition-modal-description" className={classes.margin}>
              When a player manages to play all cards from the table, so the
              table is empty, he will get a tabla, one extra point.
            </p>
          </div>
        </Fade>
      </Modal>
    </div>
  );
}
