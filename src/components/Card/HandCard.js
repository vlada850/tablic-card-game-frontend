import React from 'react';
import PropTypes from 'prop-types';
import { useDispatch, connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { clickedHandCard, emptyPileOnHandCardClick } from '../../redux/actions';

const HandCard = ({ card, active }) => {
  const dispatch = useDispatch();

  return (
    <img
      src={card.image}
      alt={card.code}
      onClick={
        active
          ? () => {
              dispatch(clickedHandCard(card));
              dispatch(emptyPileOnHandCardClick());
            }
          : () => false
      }
    />
  );
};

HandCard.propTypes = {
  card: PropTypes.object.isRequired,
  clickedHandCard: PropTypes.func.isRequired,
  emptyPileOnHandCardClick: PropTypes.func.isRequired,
  active: PropTypes.bool.isRequired,
};

const mdp = dispatch =>
  bindActionCreators(
    {
      clickedHandCard,
      emptyPileOnHandCardClick,
    },
    dispatch,
  );

export default connect(null, mdp)(HandCard);
