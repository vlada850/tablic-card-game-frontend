import React from 'react';
import PropTypes from 'prop-types';
import { useDispatch, connect } from 'react-redux';
import { clickedTableCard } from '../../redux/actions';

const TableCard = ({ card }) => {
  const dispatch = useDispatch();

  return (
    <img
      src={card.image}
      alt={card.code}
      onClick={() => dispatch(clickedTableCard(card))}
      className={card.clicked ? 'clicked' : ''}
    />
  );
};

TableCard.propTypes = {
  card: PropTypes.object.isRequired,
  dispatch: PropTypes.func.isRequired,
};

export default connect()(TableCard);

// /home/vlada/projects/tablic_front/node_modules/eslint/bin/eslint.js src/
