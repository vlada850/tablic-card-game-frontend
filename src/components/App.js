import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import './App.css';
import { fetchCards } from '../api/fetchCards';

import SideContent from './sideContent/sideContent';
import Hand from './Hand/Hand';
import Table from './Table/Table';

const App = ({
  playerOne,
  playerTwo,
  table,
  dispatch,
  playerOneIsActive,
  playerTwoIsActive,
}) => {
  useEffect(() => dispatch(fetchCards(1)), [dispatch]);

  return (
    <div className="App">
      <SideContent />
      <div className="mainContent">
        <Hand cards={playerOne} active={playerOneIsActive} />
        <Table cards={table} />
        <Hand cards={playerTwo} active={playerTwoIsActive} />
      </div>
    </div>
  );
};

App.propTypes = {
  playerOne: PropTypes.array.isRequired,
  playerOneIsActive: PropTypes.bool.isRequired,
  playerTwo: PropTypes.array.isRequired,
  playerTwoIsActive: PropTypes.bool.isRequired,
  table: PropTypes.array.isRequired,
  dispatch: PropTypes.func.isRequired,
};

const msp = state => {
  return {
    playerOne: state.playerOne,
    playerOneIsActive: state.playerOneIsActive,
    playerTwo: state.playerTwo,
    playerTwoIsActive: state.playerTwoIsActive,
    table: state.table,
  };
};

export default connect(msp)(App);
