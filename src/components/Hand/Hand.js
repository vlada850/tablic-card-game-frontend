import React from 'react';
import PropTypes from 'prop-types';
import HandCard from '../Card/HandCard';

const Hand = ({ cards, active }) => (
  <div className="hand">
    {cards.map(card => (
      <HandCard key={card.code} card={{ ...card }} active={active} />
    ))}
  </div>
);

Hand.propTypes = {
  cards: PropTypes.array.isRequired,
  active: PropTypes.bool.isRequired,
};

export default Hand;
