import React from 'react';
import PropTypes from 'prop-types';
import TableCard from '../Card/TableCard';

const Table = ({ cards }) => (
  <div className="table">
    {cards.map(card => (
      <TableCard key={card.code} card={{ ...card }} />
    ))}
  </div>
);

Table.propTypes = {
  cards: PropTypes.array.isRequired,
};

export default Table;
