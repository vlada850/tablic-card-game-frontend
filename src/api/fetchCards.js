import { setDeck } from '../redux/actions';

function fetchCards(count) {
  return (dispatch /* , getState */) => {
    dispatch({ type: 'REQUEST_STARTED' });

    fetch(
      `https://deckofcardsapi.com/api/deck/new/shuffle/?deck_count=${count}`,
    )
      .then(res => res.json())
      .then(deck =>
        fetch(
          `https://deckofcardsapi.com/api/deck/${deck.deck_id}/draw/?count=52`,
        ),
      )
      .then(res => res.json())
      .then(data => {
        dispatch(setDeck(data.cards));
        dispatch({ type: 'REQUEST_SUCCEEDED', payload: data.cards });
      })
      .catch(err => {
        console.log(err);
        dispatch({ type: 'REQUEST_FAILED', error: err });
      });
  };
}

export { fetchCards };
