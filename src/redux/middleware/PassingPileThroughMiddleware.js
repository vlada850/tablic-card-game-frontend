import { passingPileThroughMiddleware } from '../actions';

const PassingPileThroughMiddleware = store => next => action => {
  let result = next(action);

  let {
    actionTrigger,
    pile,
    thrownCard,
    playerOneIsActive,
    playerTwoIsActive,
  } = store.getState();

  if (actionTrigger) {
    store.dispatch(
      passingPileThroughMiddleware(
        pile,
        thrownCard,
        playerOneIsActive,
        playerTwoIsActive,
      ),
    );
  }

  return result;
};

export default PassingPileThroughMiddleware;
