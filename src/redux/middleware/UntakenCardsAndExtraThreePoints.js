import { playerOneUntakenCards, playerTwoUntakenCards } from '../actions';

const UntakenCardsAndExtraThreePoints = store => next => action => {
  let result = next(action);

  let {
    cards,
    playerTwo,
    playerOneLastTakenCards,
    table,
    actionTriggerForUntakenCards,
    playerOneNumberOfTakenCards,
    playerTwoNumberOfTakenCards,
  } = store.getState();

  if (
    actionTriggerForUntakenCards &&
    cards.length === 0 &&
    playerTwo.length === 0 &&
    (playerOneNumberOfTakenCards !== 0 || playerTwoNumberOfTakenCards !== 0) &&
    playerOneLastTakenCards
  ) {
    store.dispatch(
      playerOneUntakenCards(
        table,
        playerOneNumberOfTakenCards,
        playerTwoNumberOfTakenCards,
      ),
    );
  }

  if (
    actionTriggerForUntakenCards &&
    cards.length === 0 &&
    playerTwo.length === 0 &&
    (playerOneNumberOfTakenCards !== 0 || playerTwoNumberOfTakenCards !== 0) &&
    !playerOneLastTakenCards
  ) {
    store.dispatch(
      playerTwoUntakenCards(
        table,
        playerOneNumberOfTakenCards,
        playerTwoNumberOfTakenCards,
      ),
    );
  }

  return result;
};

export default UntakenCardsAndExtraThreePoints;
