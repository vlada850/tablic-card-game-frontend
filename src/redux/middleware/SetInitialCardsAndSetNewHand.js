import { setInitialCards, setNewHand } from '../actions';

const SetInitialCardsAndSetNewHand = store => next => action => {
  console.log('SetInitialCards enter');
  next(action);
  console.log('SetInitialCards exit');
  let { cards, playerOne, playerTwo } = store.getState();

  if (cards.length === 52) {
    store.dispatch(setInitialCards(cards));
  }

  if (playerOne.length === 0 && playerTwo.length === 0 && cards.length < 52) {
    if (cards.length === 0) {
      return false;
    }

    store.dispatch(setNewHand(cards));
  }
};

export default SetInitialCardsAndSetNewHand;
