import { playerOneExtraTablePoint, playerTwoExtraTablePoint } from '../actions';

const ExtraTablePoint = store => next => action => {
  let result = next(action);

  let {
    stoppingInfiniteLoopWhenTableLengthIsZero,
    playerOneIsActive,
    playerTwoIsActive,
    table,
    actionTriggerForUntakenCards,
  } = store.getState();

  if (
    stoppingInfiniteLoopWhenTableLengthIsZero &&
    actionTriggerForUntakenCards &&
    !playerOneIsActive &&
    table.length === 0
  ) {
    store.dispatch(playerOneExtraTablePoint());
  }

  if (
    stoppingInfiniteLoopWhenTableLengthIsZero &&
    actionTriggerForUntakenCards &&
    !playerTwoIsActive &&
    table.length === 0
  ) {
    store.dispatch(playerTwoExtraTablePoint());
  }

  return result;
};

export default ExtraTablePoint;
