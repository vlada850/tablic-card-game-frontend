import { combineReducers } from 'redux';
import {
  isPileOk,
  allPileSums,
  transformCard,
  calculateScoreWithCardCode,
  calculateScoreWithCardValue,
} from './utils';
import {
  SET_DECK,
  SET_INITIAL_CARDS,
  CLICKED_TABLE_CARD,
  CLICKED_HAND_CARD,
  PASSING_PILE_THROUGH_MIDDLEWARE,
  EMPTY_PILE_ON_HANDCARD_CLICK,
  SET_NEW_HAND,
  PLAYER_ONE_EXTRA_TABLE_POINT,
  PLAYER_ONE_UNTAKEN_CARDS,
  PLAYER_TWO_EXTRA_TABLE_POINT,
  PLAYER_TWO_UNTAKEN_CARDS,
} from './actions';

const cards = (state = [], action) => {
  switch (action.type) {
    case SET_DECK:
      return action.deck;
    case SET_INITIAL_CARDS:
      return action.cards.slice(16);
    case SET_NEW_HAND:
      return state.slice(12);
    default:
      return state;
  }
};

const table = (state = [], action) => {
  let pile, cardThrown;
  switch (action.type) {
    case SET_INITIAL_CARDS:
      return action.cards.slice(0, 4);
    case CLICKED_TABLE_CARD:
      return state.map(c =>
        c.code === action.card.code ? { ...c, clicked: !c.clicked } : c,
      );
    case PASSING_PILE_THROUGH_MIDDLEWARE:
      pile = action.pile.map(transformCard);
      cardThrown = transformCard(action.card);
      if (isPileOk(cardThrown, allPileSums(pile), pile)) {
        return state.filter(tc => !action.pile.some(pc => pc.code === tc.code));
      }
      return [...state.map(c => ({ ...c, clicked: false })), action.card];
    case PLAYER_ONE_UNTAKEN_CARDS:
    case PLAYER_TWO_UNTAKEN_CARDS:
      return [];
    default:
      return state;
  }
};

const playerOne = (state = [], action) => {
  switch (action.type) {
    case SET_INITIAL_CARDS:
      return action.cards.slice(4, 10);
    case SET_NEW_HAND:
      return action.cards.slice(0, 6);
    case CLICKED_HAND_CARD:
      return state.filter(c => c.code !== action.card.code);
    default:
      return state;
  }
};

const playerOneIsActive = (state = true, action) => {
  switch (action.type) {
    case CLICKED_HAND_CARD:
      return !state;
    default:
      return state;
  }
};

const playerOneLastTakenCards = (state = false, action) => {
  let pile, cardThrown;
  switch (action.type) {
    case PASSING_PILE_THROUGH_MIDDLEWARE:
      pile = action.pile.map(transformCard);
      cardThrown = transformCard(action.card);
      if (
        isPileOk(cardThrown, allPileSums(pile), pile) &&
        !action.playerOneIsActive
      ) {
        return true;
      }
      if (
        isPileOk(cardThrown, allPileSums(pile), pile) &&
        !action.playerTwoIsActive
      ) {
        return false;
      }
      return state;
    default:
      return state;
  }
};

const playerOneScore = (state = 0, action) => {
  let pile, cardThrown, arr;
  switch (action.type) {
    case PASSING_PILE_THROUGH_MIDDLEWARE:
      pile = action.pile.map(transformCard);
      cardThrown = transformCard(action.card);
      arr = [...action.pile, action.card];
      if (
        isPileOk(cardThrown, allPileSums(pile), pile) &&
        !action.playerOneIsActive
      ) {
        return (
          state +
          calculateScoreWithCardValue(arr) +
          calculateScoreWithCardCode(arr)
        );
      }
      return state;
    case PLAYER_ONE_EXTRA_TABLE_POINT:
      return state + 1;
    case PLAYER_ONE_UNTAKEN_CARDS:
      if (
        action.playerOneNumberOfTakenCards > action.playerTwoNumberOfTakenCards
      ) {
        return (
          state +
          calculateScoreWithCardValue(action.table) +
          calculateScoreWithCardCode(action.table) +
          1.5
        );
      }
      return (
        state +
        calculateScoreWithCardValue(action.table) +
        calculateScoreWithCardCode(action.table)
      );

    case PLAYER_TWO_UNTAKEN_CARDS:
      if (
        action.playerOneNumberOfTakenCards > action.playerTwoNumberOfTakenCards
      ) {
        return state + 1.5;
      }
      return state;
    default:
      return state;
  }
};

const playerOneNumberOfTakenCards = (state = 0, action) => {
  let pile, cardThrown, arr;
  switch (action.type) {
    case PASSING_PILE_THROUGH_MIDDLEWARE:
      pile = action.pile.map(transformCard);
      cardThrown = transformCard(action.card);
      arr = [...action.pile, action.card];
      if (
        isPileOk(cardThrown, allPileSums(pile), pile) &&
        !action.playerOneIsActive
      ) {
        return state + arr.length;
      }
      return state;
    default:
      return state;
  }
};

const playerTwo = (state = [], action) => {
  switch (action.type) {
    case SET_INITIAL_CARDS:
      return action.cards.slice(10, 16);
    case SET_NEW_HAND:
      return action.cards.slice(6, 12);
    case CLICKED_HAND_CARD:
      return state.filter(c => c.code !== action.card.code);
    default:
      return state;
  }
};

const playerTwoIsActive = (state = false, action) => {
  switch (action.type) {
    case CLICKED_HAND_CARD:
      return !state;
    default:
      return state;
  }
};

const playerTwoScore = (state = 0, action) => {
  let pile, cardThrown, arr;
  switch (action.type) {
    case PASSING_PILE_THROUGH_MIDDLEWARE:
      pile = action.pile.map(transformCard);
      cardThrown = transformCard(action.card);
      arr = [...action.pile, action.card];
      if (
        isPileOk(cardThrown, allPileSums(pile), pile) &&
        !action.playerTwoIsActive
      ) {
        return (
          state +
          calculateScoreWithCardValue(arr) +
          calculateScoreWithCardCode(arr)
        );
      }
      return state;
    case PLAYER_TWO_EXTRA_TABLE_POINT:
      return state + 1;
    case PLAYER_TWO_UNTAKEN_CARDS:
      if (
        action.playerTwoNumberOfTakenCards > action.playerOneNumberOfTakenCards
      ) {
        return (
          state +
          calculateScoreWithCardValue(action.table) +
          calculateScoreWithCardCode(action.table) +
          1.5
        );
      }
      return (
        state +
        calculateScoreWithCardValue(action.table) +
        calculateScoreWithCardCode(action.table)
      );

    case PLAYER_ONE_UNTAKEN_CARDS:
      if (
        action.playerTwoNumberOfTakenCards > action.playerOneNumberOfTakenCards
      ) {
        return state + 1.5;
      }
      return state;
    default:
      return state;
  }
};

const playerTwoNumberOfTakenCards = (state = 0, action) => {
  let pile, cardThrown, arr;
  switch (action.type) {
    case PASSING_PILE_THROUGH_MIDDLEWARE:
      pile = action.pile.map(transformCard);
      cardThrown = transformCard(action.card);
      arr = [...action.pile, action.card];
      if (
        isPileOk(cardThrown, allPileSums(pile), pile) &&
        action.playerOneIsActive
      ) {
        return state + arr.length;
      }
      return state;
    default:
      return state;
  }
};

const pile = (state = [], action) => {
  switch (action.type) {
    case CLICKED_TABLE_CARD:
      return state.some(c => c.code === action.card.code)
        ? state.filter(c => c.code !== action.card.code)
        : [...state, action.card];
    case EMPTY_PILE_ON_HANDCARD_CLICK:
      return [];
    default:
      return state;
  }
};

const thrownCard = (state = {}, action) => {
  switch (action.type) {
    case CLICKED_HAND_CARD:
      return action.card;
    default:
      return state;
  }
};

const actionTrigger = (state = false, action) => {
  switch (action.type) {
    case CLICKED_HAND_CARD:
      return !state;
    case PASSING_PILE_THROUGH_MIDDLEWARE:
      return !state;
    default:
      return state;
  }
};

const stoppingInfiniteLoopWhenTableLengthIsZero = (state = false, action) => {
  switch (action.type) {
    case CLICKED_TABLE_CARD:
      return true;
    case PLAYER_ONE_EXTRA_TABLE_POINT:
    case PLAYER_TWO_EXTRA_TABLE_POINT:
      return !state;
    default:
      return state;
  }
};

const actionTriggerForUntakenCards = (state = true, action) => {
  switch (action.type) {
    case PLAYER_ONE_UNTAKEN_CARDS:
    case PLAYER_TWO_UNTAKEN_CARDS:
      return false;
    default:
      return true;
  }
};

const rootReducer = combineReducers({
  cards,
  table,
  playerOneNumberOfTakenCards,
  playerTwoNumberOfTakenCards,
  playerOneScore,
  playerTwoScore,

  playerOne,
  playerOneIsActive,

  playerOneLastTakenCards,

  playerTwo,
  playerTwoIsActive,

  pile,
  thrownCard,
  actionTrigger,
  stoppingInfiniteLoopWhenTableLengthIsZero,
  actionTriggerForUntakenCards,
});

export default rootReducer;
