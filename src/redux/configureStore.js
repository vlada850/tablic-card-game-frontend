import { applyMiddleware, createStore } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import thunk from 'redux-thunk';

import monitorReducersEnhancer from './enhancers/monitorReducers';
import loggerMiddleware from './middleware/logger';
import SetInitialCardsAndSetNewHand from './middleware/SetInitialCardsAndSetNewHand';
import PassingPileThroughMiddleware from './middleware/PassingPileThroughMiddleware';
import ExtraTablePoint from './middleware/ExtraTablePoint';
import UntakenCardsAndExtraThreePoints from './middleware/UntakenCardsAndExtraThreePoints';
import rootReducer from './reducers';

export default function configureStore(preloadedState) {
  const middlewares = [
    thunk,
    ExtraTablePoint,
    UntakenCardsAndExtraThreePoints,
    loggerMiddleware,
    SetInitialCardsAndSetNewHand,
    PassingPileThroughMiddleware,
  ];
  const middlewareEnhancer = applyMiddleware(...middlewares);

  const enhancers = [middlewareEnhancer, monitorReducersEnhancer];
  const composedEnhancers = composeWithDevTools(...enhancers);

  const store = createStore(rootReducer, preloadedState, composedEnhancers);

  return store;
}
