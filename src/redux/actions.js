export const SET_DECK = 'SET_DECK';
export const SET_INITIAL_CARDS = 'SET_INITIAL_CARDS';
export const CLICKED_TABLE_CARD = 'CLICKED_TABLE_CARD';
export const CLICKED_HAND_CARD = 'CLICKED_HAND_CARD';
export const PASSING_PILE_THROUGH_MIDDLEWARE =
  'PASSING_PILE_THROUGH_MIDDLEWARE';
export const EMPTY_PILE_ON_HANDCARD_CLICK = 'EMPTY_PILE_ON_HANDCARD_CLICK';
export const SET_NEW_HAND = 'SET_NEW_HAND';
export const PLAYER_ONE_EXTRA_TABLE_POINT = 'PLAYER_ONE_EXTRA_TABLE_POINT';
export const PLAYER_ONE_UNTAKEN_CARDS = 'PLAYER_ONE_UNTAKEN_CARDS';
export const PLAYER_TWO_EXTRA_TABLE_POINT = 'PLAYER_TWO_EXTRA_TABLE_POINT';
export const PLAYER_TWO_UNTAKEN_CARDS = 'PLAYER_TWO_UNTAKEN_CARDS';

export const setDeck = deck => ({
  type: SET_DECK,
  deck,
});

export const setInitialCards = cards => ({
  type: SET_INITIAL_CARDS,
  cards,
});

export const clickedTableCard = card => ({
  type: CLICKED_TABLE_CARD,
  card: card,
});

export const clickedHandCard = card => ({
  type: CLICKED_HAND_CARD,
  card: card,
});

export const passingPileThroughMiddleware = (
  pile,
  thrownCard,
  playerOneIsActive,
  playerTwoIsActive,
) => ({
  type: PASSING_PILE_THROUGH_MIDDLEWARE,
  pile: pile,
  card: thrownCard,
  playerOneIsActive: playerOneIsActive,
  playerTwoIsActive: playerTwoIsActive,
});

export const emptyPileOnHandCardClick = () => ({
  type: EMPTY_PILE_ON_HANDCARD_CLICK,
});

export const playerOneExtraTablePoint = () => ({
  type: PLAYER_ONE_EXTRA_TABLE_POINT,
});

export const playerOneUntakenCards = (
  table,
  playerOneNumberOfTakenCards,
  playerTwoNumberOfTakenCards,
) => ({
  type: PLAYER_ONE_UNTAKEN_CARDS,
  table: table,
  playerOneNumberOfTakenCards: playerOneNumberOfTakenCards,
  playerTwoNumberOfTakenCards: playerTwoNumberOfTakenCards,
});

export const playerTwoExtraTablePoint = () => ({
  type: PLAYER_TWO_EXTRA_TABLE_POINT,
});

export const playerTwoUntakenCards = (
  table,
  playerOneNumberOfTakenCards,
  playerTwoNumberOfTakenCards,
) => ({
  type: PLAYER_TWO_UNTAKEN_CARDS,
  table: table,
  playerOneNumberOfTakenCards: playerOneNumberOfTakenCards,
  playerTwoNumberOfTakenCards: playerTwoNumberOfTakenCards,
});

export const setNewHand = cards => ({
  type: SET_NEW_HAND,
  cards: cards,
});
