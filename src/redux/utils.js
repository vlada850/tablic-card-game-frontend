const makePredicate = (i, cardThrownValue) => sum =>
  sum === i * cardThrownValue;

export const allPileSums = cards => {
  const noMoreAces = cards => {
    const withoutAces = cards.filter(card => card.value !== 1);

    return cards.length === withoutAces.length;
  };

  const add = withoutAces =>
    withoutAces.reduce((acc, card) => acc + card.value, 0);

  const flatten = arr => {
    const res = [];
    for (let i = 0; i < arr.length; i++) {
      for (let j = 0; j < arr[i].length; j++) {
        res.push(arr[i][j]);
      }
    }

    return res;
  };

  const addWithAces = cards => {
    if (noMoreAces(cards)) {
      const sum = add(cards);
      return [sum];
    }

    const copy = cards.map(card => card);
    copy.sort((l, r) => l.value - r.value);

    copy.shift();

    const arr = addWithAces(copy);
    return flatten(arr.map(num => [1 + num, 11 + num]));
  };

  return [...new Set(addWithAces(cards))];
};

export const isPileOk = (cardThrown, allPileSums, pile) => {
  const maxSum = allPileSums.reduce(
    (acc, num) => (acc < num ? num : acc),
    -Infinity,
  );
  const cardThrownValue = cardThrown.value === 1 ? 11 : cardThrown.value;

  let i;
  for (i = 1; i * cardThrownValue <= maxSum; i++) {
    if (allPileSums.some(makePredicate(i, cardThrownValue))) {
      break;
    }
  }

  if (i * cardThrownValue > maxSum) {
    return false;
  }

  return !pile.some(card => {
    const result = card.value / cardThrownValue;
    const remainder = card.value % cardThrownValue;

    return remainder === 0 && result > 1;
  });
};

export const transformCard = card => {
  const mapping = {
    ACE: 1,
    JACK: 12,
    QUEEN: 13,
    KING: 14,
  };

  const value = mapping[card.value];

  return { value: value ? value : Number(card.value) };
};

export const calculateScoreWithCardValue = arr => {
  let score = 0;
  arr.map(c => {
    switch (c.value) {
      case 'ACE':
        return score++;
      case '10':
        return score++;
      case 'JACK':
        return score++;
      case 'QUEEN':
        return score++;
      case 'KING':
        return score++;
      default:
        return 0;
    }
  });
  return score;
};

export const calculateScoreWithCardCode = arr => {
  let score = 0;
  arr.map(c => {
    switch (c.code) {
      case '2C':
        return score++;
      case '0D':
        return score++;
      default:
        return 0;
    }
  });
  return score;
};
